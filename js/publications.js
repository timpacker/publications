var fields;

function add_publication()
{
    return edit_publication(false);
}

function edit_publication($id)
{
    $.ajax({
	dataType: 'json',
	url: 'ajax.php',
	data: { action: 'get_fields' },
	method: 'post',
	success: function(data) { 
	    fields = data;
	    edit_publication_dialog($id);
	}
    });
}

function edit_publication_dialog($id)
{
    $('#dialog').remove();
    $('body').append("<div id='dialog'></div>");

    $('#dialog').append("<table>\n");

    $.each(fields, function(field,properties) {
	if(properties.title)
	{
	    title = properties.title;
	}
	else
	{
	    title = field.charAt(0).toUpperCase() + field.slice(1);
	    
	}

	$('#dialog').append("<tr>");
	$('#dialog').append("<th>"+title+"</th>");
	$('#dialog').append("<td><input type='text' id='"+field+"' name='"+field+"' /></td>");
	$('#dialog').append("</tr>\n");
    });

    $('#dialog').append("</table>\n");

    $('#dialog').dialog({
	title: 'Add publication',
	height: 'auto',
	width: 'auto',
	modal: 1,
	buttons: [
	    {
		text: "Save",
		click: function() { save_publication(); $(this).dialog('close'); }
	    },
	    {
		text: "Cancel",
		click: function() { $(this).dialog('close'); }
	    }
	]
    });
}

function save_publication()
{
    properties = new Object();
    $('#dialog input').each(function(s) {
	properties[$(this).attr('name')] = $(this).val();
    });

    $.ajax({
	dataType: 'json',
	url: 'ajax.php',
	data: { action: 'save_publication', fields: properties },
	method: 'post',
	success: function(data) { 
	    fields = data;
	    edit_publication_dialog($id);
	}
    });
    
}
