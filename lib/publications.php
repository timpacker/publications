<?php

ini_set("default_charset", 'utf-8');

error_reporting(E_ALL ^ E_NOTICE);

class Publications
{
  var $fields = array(
		      "type" => array('in_main' => 1),
		      "year" => array('in_main' => 1),
		      "title" => array('in_main' => 1),
		      "authors" => array('ris' => 'AU','multiple' => 1),
		      "editors" => array('ris' => 'A2','multiple' => 1),
		      "journal" => array('ris' => 'T2'),
		      "publisher" => array('ris' => 'PB'),
		      "volume" => array('ris' => 'VL'),
		      "issue" => array('ris' => 'IS'),
		      "pages" => array('ris' => 'SP'),
		      "isbn" => array('title' => 'ISBN','ris' => 'SN'),
		      "doi" => array('title' => 'DOI','ris' => 'DO'), 
		      );
  
  var $ristypes = array(
			"book" => 'BOOK',
			"book chapter" => 'CHAP',
			"edited book" => 'EDBOOK',
			"journal article" => 'JOUR',
			);
  
  var $mysqli;
  var $prefix;
  var $meta_statement;
  
  function __construct()
  {
    if(file_exists('conf.php'))
      {
	include('conf.php');
      }
    else
      {
	print "No conf.php file found in installation root directory.  Please create this file, based on conf.example.php";
	die();
      }
    
    $this->prefix = $mysql_prefix;
    
    $this->mysqli = new mysqli($mysql_host,$mysql_username,$mysql_password,$mysql_db);
    
    if ($this->mysqli->connect_errno) {
      echo "Failed to connect to MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
      die();
    }

    $query = "SHOW TABLES LIKE '".$this->prefix."main'";

    $result = $this->mysqli->query($query);
    if($result->num_rows < 1)
      {
	$this->initialise_tables();
	die();
      }
  }
  
  function header($title = '')
  {
    header("Content-Type: text/html; charset=UTF-8");
    
    print "<html>\n";
    print "<head>\n";
    print "  <title>Publications system".($title?" - ".$title:"")."</title>\n";
    print "  <script type='text/javascript' src='js/jquery-1.10.2.min.js'></script>\n";
    print "  <script type='text/javascript' src='js/jquery-ui-1.10.3.custom.min.js'></script>\n";
    print "  <script type='text/javascript' src='js/publications.js'></script>\n";
    print "  <link type='text/css' rel='stylesheet' href='css/smoothness/jquery-ui.min.css'>\n";
    print "  <link type='text/css' rel='stylesheet' href='css/publications.css'>\n";
    print "  <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n";
    print "</head>\n";
    print "<body>\n";
    print "<h1>Publications System</h1>\n";
    
    print "<div id='nav'>";
    print "<a href='index.php'>Home</a>";
    print "<a href='staff.php'>Staff</a>";
    print "<a href='admin.php'>Admin</a>";
    print "<a href='#' onclick='add_publication(); return false'>Add publication</a>";
    print "</div>";
    
    if($title)
      {
	print "<h2>{$title}</h2>\n";
      }
  }
  
  function footer()
  {
    print "</body>\n";
    print "</html>\n";
  }
  
  function main()
  {
    $this->header();
    
    if(isset($_GET['pid']))
      {
	$this->show_publication($_GET['pid']);
      }
    else
      {
	$this->list_all_publications();
      }
    
    $this->footer();
  }
  
  function staff()
  {
    if($_GET['staff'] && isset($_GET['ris']))
      {
	$this->staff_publications($_GET['staff'],true);
	die();
      }
    
    $this->header();
    
    if($_GET['staff'])
      {
	$this->staff_publications($_GET['staff']);
      }
    else
      {
	$this->list_staff();
      }
    
    $this->footer();
  }
  
  function list_staff()
  {
    $query = "SELECT * FROM `".$this->prefix."staff` WHERE departmental > 0 ORDER BY surname,forename";
    
    $result = $this->mysqli->query($query);
    
    print "<ul>\n";
    
    while($row = $result->fetch_assoc())
      {
	print "<li><a href='?staff={$row['sid']}'>{$row['title']} {$row['forename']} {$row['surname']}</a></li>\n";
      }
    
    print "</ul>\n";
  }
  
  function staff_details($id)
  {
    $query = "SELECT title,forename,surname FROM `".$this->prefix."staff` WHERE sid=? LIMIT 1";
    $staff_query = $this->mysqli->prepare($query);
    $staff_query->bind_param('i',$id);
    $staff = array();
    $staff_query->bind_result($staff['title'],$staff['forename'],$staff['surname']);
    $staff_query->execute();
    $staff_query->fetch();
    $staff_query->free_result();
    
    return $staff;
  }
  
  function staff_publications($id,$ris = false)
  {
    $staff = $this->staff_details($id);
    
    if(!$ris)
      {
	print "<h2>Publications for {$staff['title']} {$staff['forename']} {$staff['surname']}</h2>";
	print "<div><small><a href='?staff={$id}&ris'>Download as RIS</a></small></div>";
      }
    
    
    $pattern1 = "{$staff['forename']}%";
    $pattern2 = substr($staff['forename'],0,1).".%";
    $pattern1 = '%';

    $query = "
         SELECT
            P.*
         FROM 
            `".$this->prefix."main` P
         LEFT JOIN
            `".$this->prefix."meta` M ON (M.field IN ('authors','editors') AND M.pid=P.pid)
         LEFT JOIN
            `".$this->prefix."staff` S ON (M.value=S.sid)
         WHERE 
            surname=? AND (forename LIKE ? OR forename LIKE ?)
       ";
    
    $pub_query = $this->mysqli->prepare($query);
    $pub_query->bind_param('sss',$staff['surname'],$pattern1,$pattern2);
    $pub_query->execute();

    
    if($ris)
      {
	return $this->list_publications_ris($pub_query,$staff);      
      }
    else
      {
	return $this->list_publications($pub_query);      
      }
  }

  function get_author($id)
  {
    $query = "SELECT forename,surname FROM `".$this->prefix."staff` WHERE sid=?";
    $author_statement = $this->mysqli->prepare($query);
    $author_statement->bind_param("i",$id);
    $author_statement->bind_result($forename,$surname);
    $author_statement->execute();
    $author_statement->fetch();
    $author_statement->free_result();

    return $surname.", ".$forename;
  }
  
  function get_meta($id)
  {
    if(!($this->meta_statement))
      {
	$query = "SELECT field,value FROM `".$this->prefix."meta` WHERE pid=? ORDER BY sequence";
	
	$this->meta_statement = $this->mysqli->prepare($query);
      }

    $this->meta_statement->bind_param('i',$id);
    $this->meta_statement->bind_result($field,$value);
    $meta = array();
    $this->meta_statement->execute();
    $this->meta_statement->store_result();
    while($this->meta_statement->fetch())
      {
	if($this->fields[$field]['multiple'])
	  {
	    $meta[$field][] = $this->get_author($value);
	  }
	else
	  {
	    $meta[$field] = $value;
	  }
      }

    $this->meta_statement->free_result();

    return $meta;
  }

  function list_all_publications()
  {
    $query = "SELECT * FROM `".$this->prefix."main`";

    $statement = $this->mysqli->prepare($query);

    $statement->execute();

    return $this->list_publications($statement);
  }

  function list_publications_ris($statement, $staff)
  {
    require_once 'lib/LibRIS/src/LibRIS/RISReader.php';
    require_once 'lib/LibRIS/src/LibRIS/RISWriter.php';

    header("Content-type: application/x-research-info-systems");
    header("Content-Disposition: attachment; filename=\"{$staff['title']}_{$staff['forename']}_{$staff['surname']}.ris\"");
      
    $publications = array();

    $row = array();
    $statement->bind_result($row['pid'],$row['type'],$row['year'],$row['title']);
      
    $statement->store_result();
      
    while($statement->fetch())
      {
	$meta = $this->get_meta($row['pid']);

	$record = array(
			"TY" => array($row['type']),
			"PY" => array($row['year']),
			"TI" => array($row['title'])
			);
	foreach($this->fields as $field=>$params)
	  {
	    if($params['in_main'])
	      {
		continue;
	      }

	    if($meta[$field])
	      {
		if(is_array($meta[$field]))
		  {
		    $record[$params['ris']] = $meta[$field];
		  }
		else
		  {
		    $record[$params['ris']] = array($meta[$field]);
		  }
	      }
	  }
	$publications[] = $record;
      }

    $ris = new \LibRIS\RISWriter();
      
    print $ris->writeRecords($publications);
      
  }
      
  function list_publications($statement)
  {
    $row = array();
    $statement->bind_result($row['pid'],$row['type'],$row['year'],$row['title']);

    $statement->store_result();

    while($statement->fetch())
      {
	$meta = $this->get_meta($row['pid']);

	print "  <p><a href='index.php?pid={$row['pid']}'>";

	// Referencing style based on http://new.learnhigher.ac.uk/blog/wp-content/uploads/refandbib.pdf
	switch(strtolower($row['type']))
	  {
	  case 'book':
	  case 'edited book':
	    print implode(", ",$meta['authors'])." ({$row['year']}). <em>{$row['title']}</em>. ".
	      "{$meta['publisher']}.";
	    break;
	  case 'book chapter':
	    print implode(", ",$meta['authors'])." ({$row['year']}) '{$row['title']}' in ".
	      ($meta['editors'] ? implode(", ",$meta['editors'])." (ed.) " : '').
	      "<em>{$meta['journal']}</em>, ".
	      "{$meta['publisher']}, pp. {$meta['pages']}";
	    break;
	  case 'journal article':
	    print implode(", ",$meta['authors'])." ({$row['year']}) {$row['title']}. ".
	      "<em>{$meta['journal']}</em>, ".
	      "vol. {$meta['volume']} no. {$meta['issue']} pp. {$meta['pages']}";
	    break;
	  default:
	    print "{$row['type']} not implemented";
	    fmt($row);
	    fmt($meta);
	  }
	print "</a></p>\n";
      }
  }

  function get_publication($id)
  {
    $query = "SELECT type,year,title FROM `".$this->prefix."main` WHERE pid=? LIMIT 1";
    $get_statement = $this->mysqli->prepare($query);
    $get_statement->bind_param('i',$id);
    $get_statement->bind_result($type,$year,$title);
    $get_statement->execute();
    $get_statement->store_result();
    $get_statement->fetch();

    $details = array();

    if(trim($type))
      {
	$details['type'] = $type;
      }
    if(trim($year))
      {
	$details['year'] = $year;
      }
    if(trim($title))
      {
	$details['title'] = $title;
      }
    $details = array_merge($details,$this->get_meta($id));
    return ($details);
  }

  function show_publication($id)
  {
    $details = $this->get_publication($id);

    print "<p>";

    foreach($details as $field=>$value)
      {
	if($this->fields[$field]['title'])
	  {
	    $title = $this->fields[$field]['title'];
	  }
	else
	  {
	    $title = ucfirst($field);
	  }

	if(is_array($value))
	  {
	    $value = implode(", ",$value);
	  }

	print "<strong>{$title}:</strong> {$value}<br />\n";
      }
    print "</p>\n";
  }
    
  function admin()
  {
    $this->header("Admin");
      
    $this->upload();
      
    $this->footer();
  }
    
  function upload()
  {
    print "<h3>Upload</h3>\n";

    if($_POST['submit_upload'])
      {
	$this->process_upload();
      }
    else
      {
	$this->upload_form();
      }
  }

  function upload_form()
  {
    print "<form action='admin.php' method='post' enctype='multipart/form-data'>\n";
    print "<label for='file'>File:</label>\n";
    print "<input type='file' name='file' id='file' /><br />";
    print "<input type='submit' value='Submit' name='submit_upload' />\n";
    print "</form>\n";
  }

  function process_upload()
  {
    include("lib/PHPExcel/Classes/PHPExcel.php");
    $excel = PHPExcel_IOFactory::load($_FILES['file']['tmp_name']);
    $sheet = $excel->getSheet(0);
    $last_column = $sheet->getHighestColumn();
    $last_row = $sheet->getHighestRow();
    list($headings) = $sheet->rangeToArray("A1:{$last_column}1");
    $fields = array();

    foreach($headings as $id => $heading)
      {
	$h = trim(strtolower($heading));
	if($h == '')
	  {
	    // Remove any columns with no heading
	    unset($headings[$id]);
	  }
	else if(array_key_exists($h,$this->fields))
	  {
	    $fields[$id] = $h;
	  } 
	else
	  {
	    print "<p><strong>Unknown field: {$heading}</strong> - Column ignored.</p>";
	  }
      }

    $flookup = array_flip($fields);

    $data = $sheet->rangeToArray("A2:{$last_column}{$last_row}",NULL,TRUE,FALSE);

    $query = "INSERT INTO `".$this->prefix."main` VALUES (null,?,?,?)";

    $add_statement = $this->mysqli->prepare($query);

    if(!$add_statement)
      {
	print "Error!";
	fmt("Error: ".$this->mysqli->error);
      }
    $add_statement->bind_param('sis',$type,$year,$title);

    $meta_statement = $this->mysqli->prepare("INSERT INTO `".$this->prefix."meta` VALUES (null,?,?,?,?)");
    $meta_statement->bind_param('issi',$pid, $field, $value, $sequence);

    foreach($data as $num => $row)
      {
	print "<hr />\n";

	$type = $row[$flookup['type']];
	$title = $row[$flookup['title']];
	$year = date('Y',strtotime("01/01/".$row[$flookup['year']]));
	
	if(!$type && !$title && !$year)
	  {
	    print "No type, title or year specified on line {$row}; skipping as probable blank";
	    continue;
	  }
	
	$add_statement->execute();
	
	$pid = $this->mysqli->insert_id;
	
	foreach($fields as $id => $field)
	  {

	    $value = $row[$id];

	    if(trim($value) == '')
	      {
		continue;
	      }

	    print "{$field}: {$row[$id]}<br />\n";

	    if(in_array($field, array('type','title','year')))
	      {
		continue;
	      }
	    $sequence = 0;

	    switch($field)
	      {
	      case 'editors':
	      case 'authors':
		$authors = $this->parse_authors($value);
		foreach($authors as $id)
		  {
		    $value = $id;
		    $meta_statement->execute();
		    $sequence++;
		  }

		break;
	      default:
		$meta_statement->execute();
		break;
	      }
	  }
      }
  }

  function parse_authors($value)
  {
    $query1 = "
SELECT sid FROM
  `".$this->prefix."staff`
WHERE
  surname=?
AND
 forename=?";
    $lookup_query = $this->mysqli->prepare($query1);
    $lookup_query->bind_param('ss',$surname,$forename);
    $lookup_query->bind_result($staffid);

    $query2 = "
INSERT INTO
  `".$this->prefix."staff`
SET
  surname=?, forename=?";
    $insert_query = $this->mysqli->prepare($query2);
    $insert_query->bind_param('ss',$surname,$forename);

    $authors = array();

    $split = preg_split("/([;, ]+)/", $value, -1, PREG_SPLIT_DELIM_CAPTURE);

    $field = 'surname';
    $entry = 0;

    foreach($split as $item)
      {
	$item = trim($item);
	$lastfield = false;
	if($item == '')
	  {
	    continue;
	  }
	else if($item == 'et')
	  {
	    $field == 'surname';
	    $entry++;
	  }
	else if($item == 'al')
	  {
	    $field == 'surname';
	    $lastfield = true;
	  }
	else if($item == ';' || $item == 'and' || ($item == ',' && $field == 'forename'))
	  {
	    $entry++;
	    $field = 'surname';
	    continue;
	  }
	else if($item == ',')
	  {
	    if($authors[$entry]['surname'])
	      {
		$field = 'forename';
	      }
	    continue;
	  }
	else if(substr($item,1,1) == '.')
	  {
	    $field = 'forename';
	    if(!$authors[$entry]['surname'])
	      $entry--;
	  }
	else if($field == 'forename' && $authors[$entry]['forename'])
	  {
	    $entry++;
	    $field = 'surname';
	  }
	$authors[$entry][$field][] = $item;
	if($lastfield)
	  {
	    $entry++;
	    $field = 'surname';
	  }
      }

    $ids = array();

    foreach($authors as $author)
      {
	$forename = implode(" ",$author['forename']);
	$surname = implode(" ",$author['surname']);
	if(!$forename)
	  {
	    if(strpos($surname, " ") && $surname != 'et al')
	      {
		$surname = array_pop($author['surname']);
		$forename = implode(" ",$author['surname']);
	      }
	    else
	      {
		$forename = '';
	      }
	  }
	if(!$surname)
	  {
	    $surname = '';
	  }

	$lookup_query->execute();
	$lookup_query->store_result();

	if($lookup_query->num_rows > 0)
	  {
	    $lookup_query->fetch();
	    $ids[$staffid] = $staffid;
	    $lookup_query->free_result();
	  }
	else
	  {
	    $lookup_query->free_result();
	    $insert_query->execute();
	    $sid = $insert_query->insert_id;
	    $ids[$sid] = $sid;
	  }
      } 

    return ($ids);

  }

  function ajax()
  {
    switch($_POST['action'])
      {
      case 'get':
	$this->get_json($_POST['id']);
	break;
      case 'get_fields':
	$this->get_fields();
	break;
      case 'save_publication':
	$this->save_publication();
	break;
      }
  }

  function json_headers()
  {
    header('Content-Type: application/json');
  }

  function get_json($id)
  {
    $this->json_headers();
    $details = $this->get_publication($id);
    print json_encode($details);
  }

  function get_fields()
  {
    $this->json_headers();
    print json_encode($this->fields);
  }

  function save_publication()
  {
    $query = "INSERT INTO `".$this->prefix."main` VALUES (null,?,?,?)";
    $add_statement = $this->mysqli->prepare($query);
    $add_statement->bind_param('sis',$_POST['fields']['type'],$_POST['fields']['year'],$_POST['fields']['title']);
    $add_statement->execute();

    $pid = $this->mysqli->insert_id;

    $meta_statement = $this->mysqli->prepare("INSERT INTO `".$this->prefix."meta` VALUES (null,?,?,?,?)");
    $meta_statement->bind_param('issi',$pid, $field, $value, $sequence);

    $sequence = 0;

    foreach($this->fields as $field => $properties)
      {
	if(trim($_POST['fields'][$field]) == '')
	  continue;

	$value = trim($_POST['fields'][$field]);

	if($properties['multiple'])
	  {
	    $vals = $this->parse_authors($value);
	    $sequence = 0;
	    foreach($vals as $value)
	      {
		$meta_statement->execute();
		$sequence++;
	      }
	  }
	else
	  {
	    $meta_statement->execute();
	  }
      }

    print "Saved.";
  }

  function initialise_tables()
  {
    $queries = array(
"DROP TABLE IF EXISTS `".$this->prefix."main`;",
"CREATE TABLE `".$this->prefix."main` (
  `pid` int(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `year` int(4) NOT NULL,
  `title` mediumtext,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
"DROP TABLE IF EXISTS `".$this->prefix."meta`;",
"CREATE TABLE `".$this->prefix."meta` (
  `mid` int(20) NOT NULL AUTO_INCREMENT,
  `pid` int(20) NOT NULL,
  `field` varchar(20) NOT NULL,
  `value` varchar(255) NOT NULL,
  `sequence` tinyint(4) NOT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;",
"DROP TABLE IF EXISTS `".$this->prefix."staff`;",
"CREATE TABLE `".$this->prefix."staff` (
  `sid` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL,
  `forename` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `departmental` tinyint(1) NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;",
"INSERT INTO `".$this->prefix."staff` VALUES (1,'Professor','Alice','White',1),(2,'Dr','Kenneth','Violet',1),(3,'Dr','Eliza','Brown',1),(4,'Professor','Tami','van Green',1),(5,'Dr','May','Scarlet',1),(6,'Mr','Peter','Black',1);"
		     );

    $this->header();

    if($_GET['confirm'])
      {
	print "<p>Initialising tables....</p>";
	foreach($queries as $query)
	  {
	    fmt($query);
	    $result = $this->mysqli->query($query);
	    if(!$result)
	      {
		fmt($this->mysqli->error);
	      }
	  }
	print "Done.";
      }
    else
      {
	print "<p>Table ".$this->prefix."main does not exist.  Would you like to initialise tables using prefix ".$this->prefix." ?  This will delete any existing tables named ".$this->prefix."main, ".$this->prefix."meta or ".$this->prefix."staff.</p>";
	print "<a href='?confirm=1'>Initialise tables</a>";
      }
    $this->footer();
  }
}

function fmt($variable)
{
  print "<textarea style='width:100%;height:10em'>";
  print_r($variable);
  print "</textarea><br />";
}
